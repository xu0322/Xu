#include<graphics.h>
#include<conio.h>

#pragma comment(lib,"Winm.lib")

#define High 864
#define Width 591

IMAGE img_bk;
int position_x,position_y;
IMAGE img_planeNormal1,img_planeNormal2;

void startup()
{
	initgraph(Width,High);
	loadimge(&img_bk,"D:\\background.jpg");
	loadimge(&img_planeNormal1,"D:\\planeNormal_1.jpg");
	loadimge(&img_planeNormal2,"D:\\planeNormal_2.jpg");
	position_x = High*0.7;
	position_y = Width*0.5;
    BeginBatchDraw();
}
void show()
{
	putimge(0,0,&img_bk);
	putimge(position_x-50,position_y-60,&img_planeNormal1,NOTSRCERASE);
	putimge(position_x-50,position_y-60,&img_planeNormal2,SRCINVERT);
	FlushBatchDraw();
    sleep(2);
}
void updateWithoutInput()
{
	
}
void updateWithInput()
{
	MOUSEMSG m;
	while(MouseHit())
	{
		m=GetmouseMsg();
		//if(m.umsg==WM_MOUSEMOVE)
		{
			position_x=m.x;
			position_y=m.y;
		}
	}
}
void gameover()
{
	EndBatchDraw();
	getch();
	closegraph();
}

int main ()
{
	startup();
	while (1) 
	{
		show();
		updateWithoutInput();
		updateWithInput();
	}
	gameover; 
	return 0;
}
