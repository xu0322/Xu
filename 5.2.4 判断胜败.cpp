#include<graphics.h>
#include<conio.h>
#include<math.h>

#pragma comment(lib,"Winm.lib")

#define High 864
#define Width 591

IMAGE img_bk;
int position_x,position_y;
int bullet_x,bullet_y;
float enemy_x,enemy_y; 
IMAGE img_planeNormal1,img_planeNormal2;
IMAGE img_planeExpolde1,img_planeExpolde2;
IMAGE img_bullet1,img_bullet2;
IMAGE img_enemyPlane1,img_enemyPlane2;
int isExpolde = 0;

void startup()
{
	initgraph(Width,High);
	loadimge(&img_bk,"D:\\background.jpg");
	loadimge(&img_planeNormal1,"D:\\planeNormal_1.jpg");
	loadimge(&img_planeNormal2,"D:\\planeNormal_2.jpg");
	loadimge(&img_bullet1,"D:\\bullet1.jpg");
	loadimge(&img_bullet2,"D:\\bullet2.jpg");
	loadimge(&img_enemyPlane1,"D:\\enemyPlane1.jpg");
	loadimge(&img_enemyPlane2,"D:\\enemyPlane2.jpg");
	loadimge(&img_planeExplode1,"D:\\planeExplode1.jpg");
	loadimge(&img_planeExplode2,"D:\\planeExplode2.jpg");
	position_x = Width*0.5;
	position_y = High*0.7;
	bullet_x = position_x;
	bullet_y = -85;
	enemy_x = Width * 0.5;
	enemy_y = 10;
    BeginBatchDraw();
}
void show()
{
	putimage(0,0,&img_bk);
	if(isExpolde==0)
	{
     	putimage(position_x-50,position_y-60,&img_planeNormal1,NOTSRCERASE);
    	putimage(position_x-50,position_y-60,&img_planeNormal2,SRCINVERT);
    	putimage(bullet_x-7,bullet_y,&img_bullet1,NOTSRCERASE);
	    putimage(bullet_x-7,bullet_y,&img_bullet2,SRCINVERT);
	    putimage(enemy_x,enemy_y,&img_enemyPlane1,NOTSRCERASE);
	    putimage(enemy_x,enemy_y,&img_enemyPlane2,SRCINVERT);	
	}
	else
	{
		putimage(position_x-50,position_y-60,&img_planeExplode1,NOTSRCERASE);
			putimage(position_x-50,position_y-60,&img_planeExplode2,SRCINVERT);
	}
	FlushBatchDraw();
    sleep(2);
}
void updateWithoutInput()
{
	if(bullet_y>-25)
	    bullet_y = bullet_y - 3;
	if(enemy_y<High-25)
	    enemy_y = enemy_y + 0.5;
	else
	    enemy_y = 10;
	if(abs(bullet_x-enemy_x)+abs(bullet_y-enemy_y)<50)
	{
		enemy_x = rand()%Width;
		enemy_y = -40;
		bullet_y = -85;
	}
	if(abs(position_x-enemy_x)+abs(position_y-enemy_y)<150)	
	    isExpolde = 1;	
}
void updateWithInput()
{
	MOUSEMSG m;
	while(MouseHit())
	{
		m=GetmouseMsg();
		if(m.umsg==WM_MOUSEMOVE)
		{
			position_x=m.x;
			position_y=m.y;
		}
		else if(m.uMsg==WM_LBUTTONDOWN)
		{
			bullet_x = position_x;
			bullet_x = position_y - 85;
		}
	}
}
void gameover()
{
	EndBatchDraw();
	getch();
	closegraph();
}

int main ()
{
	startup();
	while (1) 
	{
		show();
		updateWithoutInput();
		updateWithInput();
	}
	gameover(); 
	return 0;
}
