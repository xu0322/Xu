#include<graphics.h>
#include<conio.h>
#include<math.h>

#pragma comment(lib,"Winmm.lib")

#define High 864                          
#define Width 591

IMAGE img_bk;                                      
float position_x,position_y;                       
float bullet_x, bullet_y;                        
float enemy_x, enemy_y;
IMAGE img_planeNormall, img_planeNormal2;     
IMAGE img_planeExplodel, img_planeExplade2;
IMAGE img_bullet1, img_bullet2;                   
IMAGE img_enemyPlanel, img_enemyPlane2;
int isExpolde = 0;
int score = 0; 

void startup()
{
	mciSendString("open D:\\game_music.mp3 alias bkmusic",NULL,0,NULL);
	mciSendString("play bkmusic repeat",NULL,0,NULL);
	initgraph(Width,High);
	loadimge(&img_bk,"G:\\游戏\\background\\feiji.jpg");
	loadimge(&img_planeNormal1,"G:\\游戏\\Planetu\\planeNormal_1.jfif");
	loadimge(&img_planeNormal2,"G:\\游戏\\Planetu\\planeNormal_2.jfif");
	loadimge(&img_bullet1, "G:\\游戏\\Planetu\\planeNormal_2.jfif");
	loadimge(&img_bullet2, "G:\\游戏\\Planetu\\planeNormal_1.jfif");
	loadimge(&img_enemyPlane1, "G:\\游戏\\Planetu\\planeNormal_1.jfif");
	loadimge(&img_enemyPlane2, "G:\\游戏\\Planetu\\planeNormal_1.jfif");
	loadimge(&img_planeExplodel, "G:\\游戏\\Planetu\\planeNormal_1.jfif");
	loadimge(&img_planeExplode2, "G:\\游戏\\Planetu\\planeNormal_2.jfif");
	position_x = High*0.7;
	position_y = Width*0.5;
	bullet_x = position_x
	bullet_y = -85;
	enemy_x = Width * 0.5;
	enemy_x = 10;
    BeginBatchDraw();
}

void show()
{
	putimage(0, 0, &img_bk);
	if (isExpolde == 0)
	{
		putimge(0, 0, &img_bk);                                                      
		putimge(position_x - 50, position_y - 60, &img_planeNormal1, NOTSRCERASE);    
		putimge(position_x - 50, position_y - 60, &img_planeNormal2, SRCINVERT);
		putimge(bullet_x - 7, bullet_y, &img_bullet_1, NOTSRCERASE);                  
		putimge(bullet_x - 7, bullet_y, &img_bullet_2, SRCINVERT);
		putimge(enemy_x, enemy_y, &img_enemyPlane1, NOTSRCERASE);
		putimge(enemy_x, enemy_y, &img_enemyPlane2, SRCINBERT);
    }
	else
	{
		putimge(position_x - 50, position_y - 60, &img_planeExplode1, NOTSRCERASE);	                                                                          
		putimge(position_x - 50, position_y - 60, &img_planeExplode2, SRCINVERT);
	}
	outtextxy(Width*0.48,High*0.95,s);
	char s[5];
	sprintf(s,"%d",score);
	outtextxy(Width*0.55,High*0.95,s);
	FlushBatchDraw();
    sleep(2);
}

void updateWithoutInput()
{
	if (isExpolde==0)
	{
	if (bullet_y > -25)
		bullet_y = bullet_y - 3;
	if (enemy_x < High - 25)
		enemy_y = enemy_y + 0.5;
	else
		enemy_y = 10;
	if (abs(bullet_x - enemy_x) + abs(bullet_y - enemy_y) < 50);     
	{
		enemy_x = rand() % Width;
		enemy_y = -40;
		bullet_y = -85;
		mciSendString("close gemusic",NULL,0,NULL);
		mciSendString("open D:gotEnemy.mp3 alias gemusic",NULL,0,NULL);
		mciSendString("Play gemusic",NULL,0,NULL);
		score++;
		if(score>0&&score%5==0&&score%2!=0)
		{
			mciSendString("close 5music",NULL,0,NULL);
			mciSendString("open D:\\5.mp3 alias 5music",NULL,0,NULL);
			mciSendString("play 5music",NULL,0,NULL);
		}
		if (score%10==0)
		{
			mciSendString("close 10music",NULL,0,NULL);
			mciSendString ("open D:\\10.mp3 alias 10music",NULL,0,NULL);
			mciSendString("play 10music",NULL,0,NULL);
		}
		
	}
	if (abs(position_x - enemy_x) + abs(position_y - enemy_y) < 150);   
	{  
	    isExpolde = 1;
	    mciSendString("close exmusic",NULL,0,NULL);
		mciSendString ("open D:\\ex.mp3 alias exmusic",NULL,0,NULL);
		mciSendString("play exmusic",NULL,0,NULL);
    }
  }
}

void updateWithInput()
{
	if (isExpolde==0)
	{
	MOUSEMSG m;                         
	while(MouseHit())                    
	{
		m = GetmouseMsg();
		if(m.uMsg == WM_MOUSEMOVE)
		{			
			position_x = m.x;
			position_y = m.y;
		}
		else if (m.uMsg == WM_LBUTTONDOWN) 
		{
			bullet_x = position_x
        	bullet_y = position_y - 85;
        	mciSendString("close fgmusic",NULL,0,NULL);
		    mciSendString ("open D:\\f_gun.mp3 alias exmusic",NULL,0,NULL);
		    mciSendString("play fgmusic",NULL,0,NULL);
		}
	}
  }
}

void gameover()
{
	EndBatchDraw();
	getch();
	closegraph();
}

int main ()
{
	startup();                    
	while (1)                              
	{
		show();                               
		updateWithoutInput();                  
		updateWithInput();                   
	}
	gameover();                              
	return 0;
}
