#include<graphics.h>
#include<conio.h>

#pragma comment(lib,"Winm.lib")

#define High 864
#define Width 591

IMAGE img_bk;
int position_x,position_y;
int bullet_x,bullet_y;
IMAGE img_planeNormal1,img_planeNormal2;
IMAGE img_bullet1,img_bullet2;

void startup()
{
	initgraph(Width,High);
	loadimge(&img_bk,"D:\\background.jpg");
	loadimge(&img_planeNormal1,"D:\\planeNormal_1.jpg");
	loadimge(&img_planeNormal2,"D:\\planeNormal_2.jpg");
	loadimge(&img_bullet1,"D:\\bullet1.jpg");
	loadimge(&img_bullet2,"D:\\bullet2.jpg");
	position_x = Width*0.5;
	position_y = High*0.7;
	bullet_x = position_x;
	bullet_y = -85;
    BeginBatchDraw();
}
void show()
{
	putimge(0,0,&img_bk);
	putimge(position_x-50,position_y-60,&img_planeNormal1,NOTSRCERASE);
	putimge(position_x-50,position_y-60,&img_planeNormal2,SRCINVERT);
	putimge(bullet_x-7,bullet_y,&img_bullet1,NOTSRCERASE);
	putimge(bullet_x-7,bullet_y,&img_bullet2,SRCINVERT);
	FlushBatchDraw();
    sleep(2);
}
void updateWithoutInput()
{
	if(bullet_y>-25)
	    bullet_y = bullet_y - 3;	
}
void updateWithInput()
{
	MOUSEMSG m;
	while(MouseHit())
	{
		m=GetmouseMsg();
		//if(m.umsg==WM_MOUSEMOVE)
		{
			position_x=m.x;
			position_y=m.y;
		}
		else if(m.uMsg==WM_LBUTTONDOWN)
		{
			bullet_x = position_x;
			bullet_x = position_y - 85;
		}
	}
}
void gameover()
{
	EndBatchDraw();
	getch();
	closegraph();
}

int main ()
{
	startup();
	while (1) 
	{
		show();
		updateWithoutInput();
		updateWithInput();
	}
	gameover; 
	return 0;
}
