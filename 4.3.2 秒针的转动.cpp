#include<graphics.h>
#include<conio.h>
#include<math.h>
#define High 480
#define Width 640
#definePI 3.14159


int main ()
{
	initgraph(Width,High);
	int center_x,center_y;
	center_x = Width/2;
	center_y = High/2;
	int secondLength;
	secondLength = Width/5;
	
	int secondEnd_x,secondEnd_y;
	float secondAngle = 0;
	while(1)
	{
		secondEnd_x =  center_x + secondLength * sin(secondAngle);
		secondEnd_y =  center_y - secondLength * cos(secondAngle);
		setlinestyle(PS_SOLID,2);
		setcolor(WHITE);
		line(center_x,center_y,secondEnd_x,secondEnd_y);
		sleep(100);
		setcolor(BLACK);
		line(center_x,center_y,secondEnd_x,secondEnd_y);
		secondAngle = secondAngle * 2 * PI/60;
	}
	
	
	getch();
	closegraph();
	return 0;	 
}

